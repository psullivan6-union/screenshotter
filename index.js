require('dotenv').config();
const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');

const emulateDevices = [
  'iPhone 6',
  'iPhone 6 landscape',
  'iPhone 6 Plus',
  'Nexus 5',
  'Nexus 6',
  'iPad Pro'
]

const loginLoad = async(page, path) => {
  console.log('Going to the site');
  await page.goto(`${process.env.URL_ROOT}${path}`, { waitUntil: 'networkidle0' });

  console.log('Entering email address');
  await page.click(process.env.USERNAME_SELECTOR);
  await page.keyboard.type(process.env.USERNAME);

  console.log('Entering password');
  await page.click(process.env.PASSWORD_SELECTOR);
  await page.keyboard.type(process.env.PASSWORD);

  console.log('Entering the site');
  await page.keyboard.press('Enter');

  // This is project unique ... [TODO] Make it site agnostic
  await page.waitForSelector('.page-content', {
    visible: true
  });
};

const screenshot = async(page, { width, height, path }) => {
  console.log('Taking a screenshot');
  // const watchDog = page.waitForFunction('window.innerWidth !== 0');
  await page.setViewport({ width, height });
  // await watchDog;
  await page.screenshot({ path: `screenshots/${path}`, fullPage: true });

  // page.evaluate(_ => {
  //   window.scrollBy(0, window.innerHeight);
  // });
};

const screenshotter = async(path) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  // await page.waitFor(60000);





  await loginLoad(page, path);
  await screenshot(page, {
    width: 300,
    height: 600,
    path: 'homepage-small.png'
  });

  await screenshot(page, {
    width: 800,
    height: 600,
    path: 'homepage-medium.png'
  });

  // await screenshot(page, {
  //   width: 1200,
  //   height: 600,
  //   path: 'homepage-large.png'
  // });


  // for (let device of emulateDevices) {
  //   await page.emulate(devices[device]);
  //   await loginLoad(page, path);
  //   await page.screenshot({ path: `${device}.png`, fullPage: true });
  // }

  await browser.close();
};

screenshotter('/');